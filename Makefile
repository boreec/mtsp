CC := g++
CXXFLAGS := -W -Wall  -O1 -O2
SRC_DIR := src
TARGETS := Projet

all: $(TARGETS)

Projet: $(SRC_DIR)/CostMatrix.o $(SRC_DIR)/Instance.o $(SRC_DIR)/InstanceParser.o $(SRC_DIR)/main.o
	$(CC) -o $@ $^

$(SRC_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/InstanceParser.o: $(SRC_DIR)/InstanceParser.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/Instance.o: $(SRC_DIR)/Instance.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/CostMatrix.o: $(SRC_DIR)/CostMatrix.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

clean:
	rm -f $(SRC_DIR)/*.o
	rm -f *.gnu
	rm -f *.dat

cleanall: clean
	rm -f $(TARGETS)
