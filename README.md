## Author : [Cyprien Borée](https://boreec.fr)

## Compilation

Project was built with C++, use an adequate compiler such as g++ and run Makefile :
```bash
$ make
$ make cleanall # To eventually clean temporary files and executables.
```

## Execution

### Command-line options

- `-a ARCHIVE_FILE` : Export best instances solutions (non-pareto-dominated) in a `.dat` file with a column per instance. If there are exactly 2 instances, a `ARCHIVE_FILE.dat.gnu` is generated. 
- `-d DATA_FILE` : Export instances solutions in a `.dat` with a column per instance. If there are exactly 2 instances files, a `DATA_FILE.gnu` file is generated for plotting.
- `-f FILTER` : filter solutions either OFFLINE (all at once) or ONLINE (iteratively). By default the filter is offline and if the `FILTER` is different from `ONLINE`, `OFFLINE` will be used without a warning.
- `-h` : Print a summary for command-line options.
- `-i INSTANCE_FILE` : Load a TSP instance file (see `data` folder). Repeat this option as many times as necessa ry to load instance files.
- `-p` : Display the TSP instance file matrix corner values.
- `-R QUANTITY` : Generate a specific number of random routes initially. If this option is not given, `500` random routes are generated.
- `-t` : Print execution time in ms for main algorithm steps.
- `-T SECONDS` : Set a limit in seconds for execution time.

### Examples

Load 2 instances (`data/randomA100.tsp` and `data/randomB100.tsp`) and display their corner values :
```bash
$ ./Projet -i data/randomA100.tsp -i data/randomB100.tsp  -p
```

Load 2 instances (`data/randomA100.tsp` and `data/randomB100.tsp`) with 5000 initial random routes and write into `results.dat` the evaluations found for these two instances. Since there are exactly 2 instances provided, an additional `results.dat.gnu` is generated to plot.
```bash
$ ./Projet -i data/randomA100.tsp -i data/randomB100.tsp -R 5000 -d results.dat
$ gnuplot -persist results.dat.gnu (to plot the said .gnu file)
```

Load 2 instances('data/randomA100.tsp` and `data/randomB100.tsp`) with 500 random routes and export all routes in `routes.dat` and the archive (non-pareto-dominated routes) in `archive.dat`. More over the archive is built **online** (iteratively).
```bash
$ ./Projet -i data/randomA100.tsp -i data/randomB100.tsp -d routes.dat -a archive.dat -R 500 -f ONLINE
```

Load 2 instances with 20000 random routes and export the archive after doing a 2-opt descent for 10 seconds.
```
$ ./Projet -i data/randomA100.tsp -i data/randomB100.tsp -f ONLINE -t -R 20000 -a archive.dat -T 10 -A TWO_OPT_PARETO_DESCENT
$ gnuplot -persist archive.dat.gnu
```

Load 2 instances and solve them using a scalar descent with the 2-opt operator for 10 seconds. The archive is initially
made from 20 000 random routes and exported at then in `archive2.dat`.
```bash
$ ./Projet -i data/randomA100.tsp -i data/randomB100.tsp -a archive2.dat -A TWO_OPT_SCALAR_DESCENT -T 10 -R 20000
```