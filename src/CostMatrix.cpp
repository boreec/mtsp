#include "CostMatrix.hpp"

CostMatrix::CostMatrix(const std::vector<std::vector<unsigned>> matrix) : _m(matrix) {

}

std::string CostMatrix::to_string() const {
  std::string res = "";
  res = "\t0\t1\t...\t98\t99\n";
  // first matrix line
  res += "0\t" + std::to_string(_m[0][0]) + "\t" + std::to_string(_m[0][1]);
  res += "\t...\t" + std::to_string(_m[0][_m.size() - 2]) + "\t" + std::to_string(_m[0][_m.size() - 1]) + "\n";
  // second matrix line
  res += "1\t" + std::to_string(_m[1][0]) + "\t" + std::to_string(_m[1][1]);
  res += "\t...\t" + std::to_string(_m[1][_m.size() - 2]) + "\t" + std::to_string(_m[1][_m.size() - 1]) + "\n";
  res += "...\t...\t...\t...\t...\t...\n";
  // nth-1 matrix line
  res += std::to_string(_m.size() - 2) + "\t";
  res += std::to_string(_m[_m.size() - 2][0]) + "\t" + std::to_string(_m[_m.size() - 2][1]);
  res += "\t...\t" + std::to_string(_m[_m.size() - 2][_m.size() - 2]) + "\t" + std::to_string(_m[_m.size() - 2][_m.size() - 1]) + "\n";
  // last matrix line
  res += std::to_string(_m.size() - 1) + "\t";
  res += std::to_string(_m[_m.size() - 1][0]) + "\t" + std::to_string(_m[_m.size() - 1][1]);
  res += "\t...\t" + std::to_string(_m[_m.size() - 1][_m.size() - 2]) + "\t" + std::to_string(_m[_m.size() - 1][_m.size() - 1]) + "\n";
  return res;
}

unsigned CostMatrix::f(const std::vector<unsigned>& route) const{
  unsigned sum = 0;
  for(unsigned j = 1; j < route.size(); ++j){
    sum += _m[route[j-1]][route[j]];
  }
  sum +=_m[route.back()][route.front()];
  return sum;
}
