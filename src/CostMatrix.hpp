#ifndef __COST_MATRIX_HPP__
#define __COST_MATRIX_HPP__
#include <string>
#include <vector>
class CostMatrix{
private:
  /* The matrix itself. */
  std::vector<std::vector<unsigned>> _m;
public:
  CostMatrix(const std::vector<std::vector<unsigned>> matrix);

  /*
   * Return a string with the values for each corner of the matrix.
   */
  std::string to_string() const;

  /*
   * Return the sum for every edges specified by route.
   */
  unsigned f(const std::vector<unsigned>& route) const;

  unsigned size() const {return _m.size();};
};
#endif
