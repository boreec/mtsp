#include "Instance.hpp"

Instance::Instance(const std::vector<CostMatrix>& matrices) : _matrices(matrices){

}

std::vector<unsigned> Instance::evaluate(const std::vector<unsigned>& route) const{
  std::vector<unsigned> evaluation;
  for(unsigned k = 0; k < _matrices.size(); ++k){
    evaluation.push_back(_matrices[k].f(route));
  }
  return evaluation;
}

std::vector<std::vector<unsigned>> Instance::offline_filter(const std::vector<std::vector<unsigned>>& routes) const{
  std::vector<std::vector<unsigned>> archive;

  for(unsigned i = 0; i < routes.size(); ++i){
    bool dominated = false;

    unsigned j = 0;
    while(!dominated && j < routes.size()){
      if(j != i){
	dominated = is_dominated(routes[i], routes[j]);
      }
      j++;
    }
    
    if(!dominated){
      archive.push_back(routes[i]);
    }
  }
  return archive;
}

std::vector<std::vector<unsigned>> Instance::online_filter(const std::vector<std::vector<unsigned>>& archive, const std::vector<unsigned>& route, bool& archive_changed) const{
  std::vector<std::vector<unsigned>> new_archive;
  bool dominated = false;
  unsigned i = 0;

  //if the route is not dominated by any routes in the archive add it
  //if a route in the archive is dominated by it, remove it from the archive
  while(i < archive.size()){
    dominated = is_dominated(route, archive[i]) || dominated;
    
    if(!is_dominated(archive[i], route)){
      new_archive.push_back(archive[i]);
    }
    i++;
  }
  if(!dominated){
    new_archive.push_back(route);
    archive_changed = true;
  }
  return new_archive;
}

std::vector<std::vector<unsigned>> Instance::build_archive_online(const std::vector<std::vector<unsigned>>& random_routes) const{
  std::vector<std::vector<unsigned>> archive;

  archive.push_back(random_routes[0]);
  for(unsigned i = 1; i < random_routes.size(); ++i){
    std::vector<std::vector<unsigned>> new_archive;
    bool unused;
    new_archive = online_filter(archive, random_routes[i], unused);
    archive = new_archive;
  }
  return archive;
}

bool Instance::is_dominated(const std::vector<unsigned>&r1, const std::vector<unsigned>& r2) const{
  bool dominated = true;
  unsigned eval_idx = 0;
  
  std::vector<unsigned> r1_evaluations = evaluate(r1);
  std::vector<unsigned> r2_evaluations = evaluate(r2);

  while(dominated && eval_idx <  r1_evaluations.size()){
    dominated = (r2_evaluations[eval_idx] <= r1_evaluations[eval_idx]);
    eval_idx++;
  }
  return dominated;
}

std::vector<std::vector<unsigned>> Instance::generate_random_routes(const unsigned n, const unsigned m) const{
  std::vector<std::vector<unsigned>> routes;
  routes.reserve(n);
  
  std::vector<unsigned> route(m);

  // initially fill route with numbers from 0 to m
  std::iota(std::begin(route), std::end(route), 0);

  for(unsigned i = 0; i < n; ++i){
    std::shuffle(route.begin(), route.end(), std::mt19937{std::random_device{}()});
    std::vector<unsigned> tmp = route;
    routes.push_back(tmp);
  }
  return routes;
}

/*
 * Write evaluations results for instances in a .dat file. If there are exactly two instances, 
 * an additional "results.gnu" file is generated in order to plot the evaluations as a 2D scatter
 * plot.
 * matrices : The instances (as CostMatrix)
 * routes : The routes to evaluate
 * filename : the .dat file to write.
 */
void Instance::write_dat_file(const std::vector<CostMatrix>& matrices, const std::vector<std::vector<unsigned>> routes, const std::string filename){
  std::ofstream plot_file(filename + ".gnu");
  std::ofstream data_file(filename);

  if(!data_file.is_open()){
    std::cerr << "Can't open " << filename << std::endl;
  }else{
    Instance instance(matrices);

    for(unsigned i = 0; i < routes.size(); i++){
      std::vector<unsigned> results = instance.evaluate(routes[i]);
      
      for(unsigned k = 0; k < results.size(); k++){
	data_file << results[k] << "\t";
      }
      data_file << "\n";
    }

    if(matrices.size() == 2){
      if(!plot_file.is_open()){
	std::cerr << "Can't open file results.gnu to plot evaluations." << std::endl;
      }else{
	plot_file << "plot \"" << filename <<"\" using 1:2 notitle\n";
      }
    }    
  }
}


bool Instance::archive_are_equal(const std::vector<std::vector<unsigned>>& a1,const std::vector<std::vector<unsigned>>& a2) const{
  bool equal = false;
  if(a1.size() == a2.size()){
    equal = true;
    unsigned a = 0;
    while(equal && a < a1.size()){
      std::vector<unsigned> r1 = a1[a];
      std::vector<unsigned> r2 = a2[a];

      if(r1.size() == r2.size()){
	unsigned r = 0;
	while(r < r1.size() && equal){
	  equal = r1[r] == r2[r];
	}
      }else{
	equal = false;
      }
      a++;
    }
  }
  return equal;
}
std::vector<unsigned> Instance::two_opt(const std::vector<unsigned>& route, unsigned i, unsigned j) const{
  std::vector<unsigned> new_route = route;
  new_route[i] = route[j];
  new_route[j] = route[i];
  return new_route;
}

std::vector<std::vector<unsigned>> Instance::improve_archive_with_two_opt(std::vector<std::vector<unsigned>>& archive, unsigned seconds) const{

  unsigned time_elapsed = 0;
  bool archive_improved = true;
  std::vector<std::vector<unsigned>> new_archive;
  std::vector<std::vector<unsigned>> best_archive = archive;

  auto exec_begin = std::chrono::high_resolution_clock::now();

  while(archive_improved && time_elapsed < seconds){
    unsigned a = 0;
    archive_improved = false;
    while(a < best_archive.size() && time_elapsed < seconds){

      unsigned i = 0;
      // search for a better archive as long as it is not improved.
      while(i < _matrices[0].size() && !archive_improved && time_elapsed < seconds){
	unsigned j = i + 1;
	while(j < _matrices[0].size() && !archive_improved){
	  std::vector<unsigned> route = two_opt(archive[a],i, j);
	  new_archive = online_filter(best_archive, route, archive_improved);
	  if(archive_improved){
	    best_archive = new_archive;
	  }
	  j++;
	}
	auto exec_end = std::chrono::high_resolution_clock::now();
	time_elapsed = std::chrono::duration_cast<std::chrono::seconds>(exec_end - exec_begin).count();
	i++;
      }
      a++;
    }
    
  }
  return best_archive;
}


std::vector<std::vector<unsigned>> Instance::improve_archive_with_scalarization(std::vector<std::vector<unsigned>>& archive, unsigned seconds) const{
  if(!archive.size()){
    std::cerr << "Initial archive must not be empty." << std::endl;
    std::abort();
  }
  
  std::vector<std::vector<unsigned>> best_archive = archive;
  unsigned time_elapsed = 0;
  auto exec_begin = std::chrono::high_resolution_clock::now();

  while(time_elapsed < seconds){
    
    // Select a random solution in the archive.
    unsigned random_archive_solution = 0;
    if(best_archive.size() > 1){
      std::default_random_engine generator;
      std::uniform_int_distribution<int> int_distribution(0,best_archive.size()-1);
      random_archive_solution = int_distribution(generator);
    }
    std::vector<unsigned> route = best_archive[random_archive_solution];
    std::vector<unsigned> best_route = scalar_descent_with_two_opt(route);

    bool archive_changed;
    std::vector<std::vector<unsigned>> new_archive = online_filter(best_archive, best_route, archive_changed);
    if(archive_changed)
      best_archive = new_archive;
    auto exec_end = std::chrono::high_resolution_clock::now();
    time_elapsed = std::chrono::duration_cast<std::chrono::seconds>(exec_end - exec_begin).count();
    
  }
  return best_archive;
}

std::vector<unsigned> Instance::scalar_descent_with_two_opt(const std::vector<unsigned>& route) const{
  std::vector<unsigned> best_route = route;
  std::vector<unsigned> best_evaluation = evaluate(best_route);
  std::vector<float> weights = get_random_weights(_matrices.size());
  float best_score = aggregate (weights, best_evaluation);

  for(unsigned i = 0; i < route.size(); i++){
    for(unsigned j = 0; j < route.size(); j++){
      if(i == j)
	continue;
      std::vector<unsigned> new_route = two_opt(best_route, i, j);
      std::vector<unsigned> new_evaluation = evaluate(new_route);
      float new_score = aggregate(weights, new_evaluation);
      if(new_score < best_score){
	best_score = new_score;
	best_route = new_route;
	best_evaluation = new_evaluation;
      }
    }
  }
  return best_route;
}

std::vector<float> Instance::get_random_weights(const unsigned n) const{
  std::vector<float> weights;
  std::default_random_engine generator;
  std::uniform_real_distribution<float> float_distribution(0,1);
  for(unsigned w = 0; w < n; ++w){
    weights.push_back(float_distribution(generator));
  }
  return weights;
}

float Instance::aggregate(const std::vector<float>& weights, const std::vector<unsigned>& values) const{
  float result = 0;
  for(unsigned w = 0; w < weights.size(); ++w){
    result += (weights[w] * values[w]);
  }
  return result;
}
