#ifndef __INSTANCE_HPP__
#define __INSTANCE_HPP__
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <random>
#include <vector>
#include "CostMatrix.hpp"
class Instance{
private:
  std::vector<CostMatrix> _matrices;
public:
  /*
   * Build an Instance from a set of cost matrices.
   */
  Instance(const std::vector<CostMatrix>& matrices);

  /*
   * Generate random routes based on the cities in instance files.
   * n : The unsigned number of routes to generate.
   * m : The unsigned number of cites.
   */
  std::vector<std::vector<unsigned>> generate_random_routes(const unsigned n, const unsigned m) const;

  /*
 * Write evaluations results for instances in a .dat file. If there are exactly two instances, 
 * an additional "results.gnu" file is generated in order to plot the evaluations as a 2D scatter
 * plot.
 * matrices : The instances (as CostMatrix)
 * routes : The routes to evaluate
 * filename : the .dat file to write.
 */
  void write_dat_file(const std::vector<CostMatrix>& matrices, const std::vector<std::vector<unsigned>> routes, const std::string filename);

  /*
   * Compute and return a vector of every objective functions for the given route.
   */
  std::vector<unsigned> evaluate(const std::vector<unsigned>& route) const;

  /*
   * Filter out non-pareto optimal routes and return the result.
   */
  std::vector<std::vector<unsigned>> offline_filter(const std::vector<std::vector<unsigned>>& routes) const;

  /*
   * Add a route to the archive if is not Pareto-dominated by any other routes in that archive.
   * If some routes in the archive are dominated by it, they are removed.
   */
  std::vector<std::vector<unsigned>> online_filter(const std::vector<std::vector<unsigned>>& archive, const std::vector<unsigned>& route, bool& archive_improved) const;

  /*
   * Build an archive iteratively on a fixed amount of random routes.
   * Each iteration, the archive is updated with an online filter.
   * The final archive is returned afterward.
   */
  std::vector<std::vector<unsigned>> build_archive_online(const std::vector<std::vector<unsigned>>& random_routes) const;
  /*
   * Return true if the route r1 is dominated on at least one dimension by r2.
   * (Dominated means that the evaluation is better for said dimension for r2)
   */
  bool is_dominated(const std::vector<unsigned>& r1, const std::vector<unsigned>& r2) const;

  /*
   * Apply a 2-opt swap on a given route.
   * (swap city I with city J)
   */
  std::vector<unsigned> two_opt(const std::vector<unsigned>& route, unsigned i, unsigned j) const;

  /*
   * Take every route found in the archive and do a two-opt descent on them as long as
   * a better solution has been found or until a time limit has been reached.
   */
  std::vector<std::vector<unsigned>> improve_archive_with_two_opt(std::vector<std::vector<unsigned>>& archive, unsigned seconds) const;

  /*
   * Return true if the two archive are exactly equal (they contain the same routes),
   * false otherwise.
   */
  bool archive_are_equal(const std::vector<std::vector<unsigned>>& a1, const std::vector<std::vector<unsigned>>& a2) const;
};
#endif
