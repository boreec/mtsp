#include "InstanceParser.hpp"

CostMatrix InstanceParser::parse(const std::string filename){
  std::ifstream instance_file(filename);
  if(!instance_file.is_open()){
    std::cerr << "Error opening " << filename << std::endl;
    exit(EXIT_FAILURE);
  }
  
  std::vector<std::vector<unsigned>> matrix;
  std::string line;
  unsigned line_number = 0;
  unsigned instance_size = 0;
  unsigned m_column = 0;
  unsigned m_line = 0;
  while(getline(instance_file, line)){

    // Once instance_size is found, an empty matrix (full of 0s) is generated.
    if(!instance_size){
      size_t size_found = line.find("DIMENSION");
      if(size_found != std::string::npos){
	std::string dimension = line.substr(std::string("DIMENSION : ").size());
	if(dimension.back() == '\n'){
	  dimension.pop_back();
	}
	instance_size = std::stoul(dimension);
	for(unsigned i = 0; i < instance_size; ++i){
	  std::vector<unsigned> row(instance_size,42);
	  matrix.push_back(row);
	}
      }
    }

    // Once header has been read.
    if(line_number >= 7){
      if(m_column == instance_size){
	m_line++;
	m_column = m_line;
      }
      unsigned data = std::stoul(line);
      matrix[m_line][m_column] = data;
      matrix[m_column][m_line] = data;
      m_column++;
      
    }
    line_number++;
  }
  return CostMatrix(matrix);
}
