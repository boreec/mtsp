#ifndef __INSTANCE_PARSER_HPP__
#define __INSTANCE_PARSER_HPP__
#include <fstream>
#include <iostream>
#include <string>
#include "CostMatrix.hpp"
class InstanceParser{
public:
  /*
   * Return a cost matrix from an instance file.
   */
  static CostMatrix parse(const std::string filename);
};

#endif
