#include <chrono>
#include <iomanip>
#include <iostream>
#include <unistd.h>
#include "CostMatrix.hpp"
#include "Instance.hpp"
#include "InstanceParser.hpp"

/*
 * Display the given execution time for a given task name. Time is displayed is milliseconds, so if
 * execution is faster than 1ms, 0ms is printed. The final text attempts to be aligned on stdout.
 */
void print_execution_time(std::string step, std::chrono::duration<long int,std::ratio<1, 1000000000>> time){
  auto ms_time = time/std::chrono::microseconds(1000);
  auto dec = time/std::chrono::microseconds(1) % 100;
  std::string extra_zero = "";
  if(dec < 10)
    extra_zero = "0";
  unsigned step_len = step.size();
  unsigned width = 50;
  std::cout << step << std::setw(width - step_len) << ms_time << "." << dec << extra_zero << "ms" << std::endl;
}

void print_usage(){

  std::cout << "Command-line options and arguments : " << std::endl;
  std::cout << "-a ARCHIVE_FILE  : Export the best evaluations found in a .dat file" << std::endl;
  std::cout << "-A ALGORITHM     : Use an algorithm to solve mTSP. Either TWO_OPT_PARETO_DESCENT or TWO_OPT_SCALAR_DESCENT." << std::endl;
  std::cout << "-d DATA_FILE     : Export all instances evaluations in a .dat file" << std::endl;
  std::cout << "-f FILTER        : Filter using either OFFLINE or ONLINE." << std::endl;
  std::cout << "-h               : Display this message." << std::endl;
  std::cout << "-i INSTANCE_FILE : Import instance file." << std::endl;
  std::cout << "-p               : Display corners values for matrices." << std::endl;
  std::cout << "-R QUANTITY      : Generate a specific number of routes initially." << std::endl;
  std::cout << "-t               : Print execution time in ms for main steps" << std::endl;
  std::cout << "-T TIME_LIMIT    : The time in seconds that is used as a limit for execution." << std::endl;
}

/*
 * Parse command line arguments and call related functions.
 */
void parse_args(int argc, char* argv[]){
  bool FILTER_ONLINE = false;
  bool PRINT_MATRICES = false;
  bool PRINT_EXECUTION_TIME = false;
  std::string ARC_FILE = "";
  std::string DAT_FILE = "";
  unsigned ROUTES_QUANTITY = 500;
  unsigned TIME_LIMIT = 10;
  std::string ALGORITHM = "";
  std::vector<std::string> instance_filenames;
  
  int opt;
  while ((opt = getopt(argc,argv,"A:a:d:f:hi:pR:tT:")) != EOF){
    switch(opt){
    case 'a':
      ARC_FILE = optarg;
      break;
    case 'A':
      ALGORITHM = optarg;
      break;
    case 'd' :
      DAT_FILE = optarg;
      break;
    case 'f' :
      if(std::string(optarg) == "ONLINE"){
	FILTER_ONLINE = true;
      }
      break;
    case 'h' :
      print_usage();
      break;
    case 'i':
      instance_filenames.push_back(optarg);
      break;
    case 'p':
      PRINT_MATRICES = true;
      break;
    case 'R' :
      ROUTES_QUANTITY = std::stoul(optarg);
    break;
    case 't':
      PRINT_EXECUTION_TIME = true;
      break;
    case 'T':
      TIME_LIMIT = std::stoul(optarg);
      break;
    case '?': 
      print_usage();
      break;
    default:
      print_usage();
      break;
    }
  }
  if(!instance_filenames.size()){
    std::cerr << "At least one instance file is expected! See usage: " << std::endl;
    print_usage();
  }else{
    std::vector<CostMatrix> matrices;
    for(unsigned j = 0; j < instance_filenames.size();++j){
      matrices.push_back(InstanceParser::parse(instance_filenames[j]));
    }
    Instance inst = Instance(matrices);
    
    if(PRINT_MATRICES){
      for(unsigned c = 0; c < matrices.size(); ++c){
	std::cout << matrices[c].to_string() << std::endl;
      }
    }

    std::vector<std::vector<unsigned>> archive;
    auto generate_rr_begin = std::chrono::high_resolution_clock::now();
    std::vector<std::vector<unsigned>> routes = inst.generate_random_routes(ROUTES_QUANTITY, matrices[0].size());
    auto generate_rr_end = std::chrono::high_resolution_clock::now();

    if(PRINT_EXECUTION_TIME)
      print_execution_time("generate random routes", generate_rr_end - generate_rr_begin);
    if(FILTER_ONLINE){
      auto filter_begin = std::chrono::high_resolution_clock::now();
      archive = inst.build_archive_online(routes);
      auto filter_end = std::chrono::high_resolution_clock::now();
      if(PRINT_EXECUTION_TIME)
	print_execution_time("Filter online", filter_end - filter_begin);
    }else{
      archive = inst.offline_filter(routes);
    }

    
    if(DAT_FILE != ""){      
      inst.write_dat_file(matrices, routes, DAT_FILE);
    }

    if(ALGORITHM == "TWO_OPT_PARETO_DESCENT"){
      archive = inst.improve_archive_with_two_opt(archive, TIME_LIMIT);

    }else if(ALGORITHM == "TWO_OPT_SCALAR_DESCENT"){
      archive = inst.improve_archive_with_scalarization(archive, TIME_LIMIT);
    }else{
      std::cerr << "No algorithm use to solve mTSP!" << std::endl;
    }
    
    if(ARC_FILE != ""){
      inst.write_dat_file(matrices, archive, ARC_FILE);
    }
  }
}

int main(int argc, char* argv[]){
  if(argc == 1){
    print_usage();
  }else{
    parse_args(argc, argv);
  }
  return 0;
}
